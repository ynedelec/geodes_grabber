# Steps

- Load in VS Code
- Open in remote container
- Prepare georef file, in bash shell: `curl --compressed -sS -X GET "https://geodes.santepubliquefrance.fr/GC_refdata.php?nivgeo=dep&extent=fra&lang=fr&prodhash=600f99d2" | jq -r '[.content.territories.codgeo,.content.territories.libgeo] | transpose | map({(.[0]): .[1] }) | add' > georef.json`
- Call script in the terminal `bash app.sh`

# Misc.
Convert to csv, in bash shell: `cat <(echo "date,department,positive,tested")  <(cat data.json | jq -r '.[] | [.date, .dep, .p, .t] | @tsv' | tr '\t' ',')`
