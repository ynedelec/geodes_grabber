#!/bin/bash
# Spatial ref JSON
departments_raw=$(jq -r 'keys | @csv' georef.json | tr -d '"');

# Explode string into bash array
IFS=',' read -ra departments <<< "$departments_raw";

file="data.tmp";
[ -e "$file" ] && rm "$file";
touch $file;

# while true
# do
# Iterate through departments
for department in "${departments[@]}"
do
    echo "Collecting data for department $department";
    response=$(curl --compressed -sS -X GET "https://geodes.santepubliquefrance.fr/GC_infosel.php?lang=fr&allindics=1&nivgeo=dep&view=map2&codgeo=$department&dataset=sp_pos_quot&indic=tx_pos_quot");
    echo $response | jq '.content.data.sp_pos_quot[] | select(.territory=="dep@'$department'") | { date: .jour, dep: "'$department'", ageClass: .cl_age90, p: .p, t: .t }' >> $file;
done
jq -s . $file > "data.json";
# rm $file;
# echo "Created json file, now sleeping for 12 hours...";
# sleep 43200;
# done
